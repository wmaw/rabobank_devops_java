# Assignment for Rabobank

## Setup / Prereqs
#### Postman Collection
I've made a small postman collection, which I made to learn how to interact with Wiremock. 
You can use it to interact with the container directly.

[https://www.getpostman.com/collections/aff6e13b3d96832cc0fd](https://www.getpostman.com/collections/aff6e13b3d96832cc0fd)

#### Wiremock image
* Pull the latest Wiremock image: `docker pull rodolpheche/wiremock:2.25.1`
    * I cheated and used somebody else's image.


## Running the application(s)
> Run the following command(s) from the project root `~/rabobank_devops_java`


* Run the docker image:
    * Stubs are mounted from `~/rabobank_devops_java/wiremock/mappings` to the image's volume at `/home/wiremock`
    * `docker run --rm -d -p 8080:8080 -p 8443:8443 --name wiremock_rabo -v $PWD/wiremock:/home/wiremock rodolpheche/wiremock:2.25.1`

* Run the application:
    * Use the following command, or run it through an IDE of your choice: `./mvnw spring-boot:run`
    
* Open your browser:
    * [http://localhost:8085/](http://localhost:8085/) 


## Testing the Application
> Run the following command(s) from the project root `~/rabobank_devops_java`

* You can run tests either through your IDE of choice, 
or by executing the following: `./mvnw test`

#### Testing Failed
Uh oh.

It's probably the `/random_user` http test. This test calls `randomuser.me/api` to see if it's available.
That API occasionally throws errors, so it's probably caused by that.


## Limitations and Assumptions
1. I didn't write a dedicated error handler for the application.
    * Since this is a small application, I instead let Spring Boot's fallback error handler do the lifting. If the application was larger/more complex, I would've written a global handler (at minimum). 
        * A possible alternative to my laziness, would've been to add a 'local' exception handler to `UserController`. I decided not to, as that would cause maintanability issues if the program were to be expanded (that particular handler would only be available to that particular controller).
2. To save time, I cheated and used a pre-made docker image for wiremock.
    * By feeding the run command some options, I've made it usable for my (our) purposes here.
3. I serve web content using Thymeleaf. It was fast, easy, and operates similar to some JS frameworks that I'm familiar with; I could've made the HTML prettier.
4. I hope I haven't left any todos lying around....

## Resources and References
* Used the following docker image for Wiremock: 
    * https://hub.docker.com/r/rodolpheche/wiremock/
* Other resources consulted:
    * https://medium.com/@jw_ng/mocking-with-wiremock-and-docker-1f1601bd10e4
    * https://www.softwaretestinghelp.com/wiremock-stubbing-verification-proxying/