package mwood.rabo.application.spring.controller;

import mwood.rabo.application.spring.Application;
import mwood.rabo.application.spring.model.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {

    @Value("${backend.endpoint.url}")
    private String apiPath;

    private static final Logger log = LoggerFactory.getLogger(Application.class);
    private static RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/users")
    public String user(
        @RequestParam(name="first_name", required=false) String first_name,
        @RequestParam(name="last_name", required=false) String last_name, Model model) {

            log.info("====== call to wiremock stub @ users ======");

            String apiRouteContext = "/users";
            Map<String, String> queryParams = new HashMap<>();

            if (first_name != null) {
                queryParams.put("first_name", first_name);
            }

            if (last_name != null) {
                queryParams.put("last_name", last_name);
            }

            /**
             * This is ugly, I know. It makes me sad.
             * I did this because the stub has different endpoints, whether it's a "query" or a basic get-all request.
             * query=`/user?blah=.....` && getall=`/users`
             * I couldn't figure out how do make a stub accept both query parameters and no-query parameters.
             */
            if (!queryParams.isEmpty()) {
                apiRouteContext = "/user";
            }

            // Add query parameters
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiPath + apiRouteContext);
            for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                builder.queryParam(entry.getKey(), entry.getValue());
            }

            ApiResponse response = restTemplate.getForObject(builder.toUriString(), ApiResponse.class);
            model.addAttribute("response", response);

            return "user";
    }

    @GetMapping("/random_user")
    public String user(Model model) {

        log.info("====== call to wiremock stub @ proxy ======");

        String apiRouteContext = "/random_user";

        ApiResponse response = restTemplate.getForObject(apiPath + apiRouteContext, ApiResponse.class);
        model.addAttribute("response", response);

        return "user";
    }


}
