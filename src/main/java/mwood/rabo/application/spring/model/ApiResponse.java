package mwood.rabo.application.spring.model;

import mwood.rabo.application.spring.model.User.User;

public class ApiResponse {

    public User[] results;

    public User[] getResults() {
        return results;
    }

    public void setResults(User[] results) {
        this.results = results;
    }

}
