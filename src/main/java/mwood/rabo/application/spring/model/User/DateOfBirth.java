package mwood.rabo.application.spring.model.User;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateOfBirth {

    private String date;
    private Integer age;

    public DateOfBirth() {
    }

    /**
     * Parses date from 'yyyy-dd-MMT00:00:00.SSSZ' to 'dd-MM-yyyy'
     * @param date string
     * @return string
     * @throws ParseException
     */
    public String convertDate(String date) throws ParseException {
        String parsedDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")).toString();
        return parsedDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException{
//        this.date = convertDate(date);    // Not working as expected
        this.date = date;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "DateOfBirth{" +
                "date=" + date +
                ", age='" + age + '\'' +
                '}';
    }
}
