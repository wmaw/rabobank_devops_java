package mwood.rabo.application.spring.model.User;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)     // ignores any properties in object not specified in this class
public class User {

    // variables must match JSON keys
    private String gender;
    private Name name;
    private DateOfBirth dob;
    private Picture picture;

    public User() {
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public DateOfBirth getDob() {
        return dob;
    }

    public void setDob(DateOfBirth dob) {
        this.dob = dob;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "User{" +
            "gender='" + gender + '\'' +
            ", name=" + name +
            ", dob=" + dob +
            ", picture=" + picture +
            '}';
    }

}
