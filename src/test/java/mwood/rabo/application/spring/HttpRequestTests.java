package mwood.rabo.application.spring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpRequestTests {

    @Value("${backend.endpoint.url}")
    private String backEndService;

    @Value("${thirdparty.endpoint.randomuser.url}")
    private String randomUserMe;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Checks if "backend" is running, and has loaded stub mappings.
     * @throws Exception
     */
    @Test
    public void checkBackendConnection() throws Exception {
        assertThat(this.restTemplate.getForObject(backEndService + "/__admin/mappings", String.class)).contains("mappings").isNotEmpty();
    }

    /**
     * Checks if randomuser.me API is alive
     * @throws Exception
     */
    @Test
    public void check3rdPartyApiIsAlive() throws Exception {
        assertThat(this.restTemplate.getForObject(randomUserMe + "/api/?format=json&noinfo", String.class)).isNotEmpty();
    }

    /**
     * Checks if index is returned on load
     * @throws Exception
     */
    @Test
    public void indexShouldReturnDefaultTemplate() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/", String.class)).contains("Get a user!");
    }

    /**
     * Uses user query route to check user template with static data
     * @throws Exception
     */
    @Test
    public void userShouldReturnDefaultTemplate() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/users?first_name=Daria&last_name=Hartl", String.class)).contains("Hartl");
    }

    /**
     * Uses users route to check user template is populated 3 times
     * @throws Exception
     */
    @Test
    public void usersShouldReturnThreeResults() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/users", String.class)).contains("Dean");
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/users", String.class)).contains("Perrin");
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/users", String.class)).contains("Flores");
    }

    /**
     * Uses random_user route to check user template with dynamic data
     * @throws Exception
     */
    @Test
    public void randomUserShouldReturnUser() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/random_user", String.class)).contains("Your user:");
    }

}
